#!/usr/bin/python3.7
import json
import logging
from threading import Thread

from google.cloud import pubsub_v1

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)
logging.getLogger("pika").setLevel(logging.INFO)

class VideoConversionMessaging(Thread):
    def __init__(self, _config_, converting_service):

        Thread.__init__(self)
        subscriber = pubsub_v1.SubscriberClient()

        subscription_name = 'projects/tppaas/subscriptions/conversion'.format(
            project_id='tppaas', sub='conversion')

        def callback(message):
            message.ack()
            self.converting_service = converting_service
            self._on_message_(message.data)

        future = subscriber.subscribe(subscription_name, callback)

        try:
            future.result()
        except KeyboardInterrupt:
            future.cancel()

    def on_message(self, channel, method_frame, header_frame, body):
        logging.info(body)
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])

    def _on_message_(self,  body):
        logging.info(body)
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])


    def stop_consuming(self):
        logging.info("Stops consuming on message bus")
        # self.channel.stop_consuming()
        self.consuming = "_IDLE_"

    def start_consuming(self):
        logging.info("Starts consuming on message bus")
        #self.channel.start_consuming()
        # self.rendez_vous.put("_CONSUMING_")
        self.consuming = "_CONSUMING_"
        # self.start()

    def is_consuming(self):
        return self.consuming